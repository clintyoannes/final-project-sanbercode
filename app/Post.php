<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tulisan', 'gambar', 'gambar_capt', 'quotes', 'like', 'dislike', 'id_users',
    ];

    public function comments()
    {
    	return $this->hasMany('App\Comment');
    }

    public function profiles()
    {
    	return $this->belongsTo('App\Profile');
    }
}
