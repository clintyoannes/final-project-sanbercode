<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SanbercodeController extends Controller
{
    public function login(){
        return view('login');
    }

    public function home(){
        return view('home');
    }

    public function profile(){
        return view('profile');
    }
}
