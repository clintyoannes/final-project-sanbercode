<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nama', 'profile_pict', 'followers', 'following', 'id_users',
    ];

    public function users()
    {
    	return $this->belongsTo('App\User');
    }

    public function posts()
    {
    	return $this->hasMany('App\Post');
    }

    public function comments()
    {
    	return $this->hasMany('App\Comment');
    }
}
