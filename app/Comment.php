<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'isi', 'like', 'dislike', 'id_users', 'id_posts',
    ];

    public function posts()
    {
    	return $this->belongsTo('App\Post');
    }

    public function profiles()
    {
    	return $this->belongsTo('App\Profile');
    }
}
