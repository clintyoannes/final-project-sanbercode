<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('isi');
            $table->integer('like')->nullable();
            $table->integer('dislike')->nullable();
            $table->unsignedBigInteger('id_users');
            $table->unsignedBigInteger('id_posts');
            $table->foreign('id_users')->references('id')->on('users');
            $table->foreign('id_posts')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_comments');
    }
}
